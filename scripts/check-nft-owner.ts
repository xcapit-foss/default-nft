import dotenv from "dotenv";
import { ethers } from "hardhat";
import { DefaultNFT } from "../typechain";

dotenv.config();

const OWNER_ADDRESS = ''; // caso que tiene nft

const { API_URL, PRIVATE_KEY, PUBLIC_KEY } = process.env;

const provider = new ethers.providers.JsonRpcProvider(API_URL);

const abi = require("../artifacts/contracts/NFT16921.sol/NFT16921.json").abi;
const contractAddress = "";
const wallet = new ethers.Wallet(`0x${PRIVATE_KEY}`, provider);

const contract = (new ethers.Contract(contractAddress, abi, wallet)) as DefaultNFT;

contract.walletOfOwner(OWNER_ADDRESS).then(console.log);
