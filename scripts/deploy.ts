import dotenv from "dotenv";
import { ethers } from "hardhat";


dotenv.config();

const { CONTRACT_NAME, CONTRACT_SYMBOL } = process.env;

async function main() {
    const defaultNFT = await ethers.getContractFactory("DefaultNFT")

    const contract = await defaultNFT.deploy(CONTRACT_NAME, CONTRACT_SYMBOL)
    console.log("Contract deployed to address:", contract.address)
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
