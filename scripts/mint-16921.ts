import dotenv from "dotenv";
import { ethers } from "hardhat";

dotenv.config();

const {API_URL, PRIVATE_KEY, PUBLIC_KEY } = process.env;

const provider = new ethers.providers.JsonRpcProvider(API_URL);
const abi = require("../artifacts/contracts/NFT16921.sol/NFT16921.json").abi;
const contractAddress = "";

const wallet = new ethers.Wallet(`0x${PRIVATE_KEY}`, provider);

const contract = new ethers.Contract(contractAddress, abi, wallet);
const ipfsHash = "";

// contract.mint(PUBLIC_KEY, [ipfsHash]).then((res:any) => console.log(res));

function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

const cmon = 0;
const out_ = 0;

async function mint() {
	for (let out_index = 0; out_index < out_; out_index++) {
		const hashes: any = [];
		for (let i = 0; i < cmon; i++) {
			hashes[i] = ipfsHash;
		}
		const result = await contract.mint(PUBLIC_KEY, hashes);
		console.log(result);
		await delay(7000 + out_index);
	}
}

mint().then(() => console.log('fin'));
