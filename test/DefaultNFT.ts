import { BigNumber } from "@ethersproject/bignumber";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { DefaultNFT } from "../typechain";


describe("Token Contract", function() {
    let Token;
    let defaultNFT: DefaultNFT;
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    let addrs: SignerWithAddress[];

    this.beforeEach(async function() {
        Token = await ethers.getContractFactory("DefaultNFT");
        [owner, addr1, ...addrs] = await ethers.getSigners();
        defaultNFT = (await Token.deploy('aContractName', 'aContractSymbol')) as DefaultNFT;
    });

    describe("Deployment", function() {
        it("Should set the right owner", async function() {
            expect(await defaultNFT.owner()).to.equal(owner.address);
        });
    });

    describe("Mint", function() {
        it("Should mint 2 nft", async function() {
            const hashes = ["1", "2"];
            await defaultNFT.mint(owner.address, hashes);

            expect(await defaultNFT.totalSupply()).to.equal(hashes.length);
        });

        it("Should not mint if I am not the owner", async function() {
            await expect(defaultNFT.connect(addr1).mint(owner.address, ["1", "2"]))
                .to.be.revertedWith('Ownable: caller is not the owner');
        });

        it("Should mint one nft", async function() {
            await defaultNFT.mintOne(owner.address, "1");

            expect(await defaultNFT.totalSupply()).to.equal(1);
        });

        it("Should mint one nft and after mint two at same time", async function() {
            await defaultNFT.mintOne(owner.address, "1");
            await defaultNFT.mint(owner.address, ["1", "2"]);

            expect(await defaultNFT.totalSupply()).to.equal(3);
        });
    });

    describe("Tokens query", function() {
        it("Should own 1 nft after mint it", async function() {
            await defaultNFT.mintOne(owner.address, "2");

            const tokenIds = await defaultNFT.walletOfOwner(owner.address);

            expect(tokenIds[0]).to.equal(BigNumber.from(0));
            expect(tokenIds.length).to.equal(1);
        });

        it("Should own 2 nft after mint it", async function() {
            const hashes = ["1", "2"];
            const expectedValue = [BigNumber.from(0), BigNumber.from(1)];
            await defaultNFT.mint(owner.address, hashes);

            const tokenIds = await defaultNFT.walletOfOwner(owner.address);

            expect(tokenIds[0]).to.equal(expectedValue[0]);
            expect(tokenIds[1]).to.equal(expectedValue[1]);
            expect(tokenIds.length).to.equal(expectedValue.length);
        });

        it("Should not own a nft after mint it", async function() {
            const hashes = ["1", "2"];
            await defaultNFT.mint(owner.address, hashes);

            expect((await defaultNFT.walletOfOwner(addr1.address)).length).to.equal(0);
        });

        it("Should get token uri of a minted nft", async function() {
            const hashes = ["1"];
            const expectedValue = (await defaultNFT.baseURI()) + hashes[0];
            await defaultNFT.mint(owner.address, hashes);

            expect(await defaultNFT.tokenURI(0)).to.equal(expectedValue);
        });

        it("Should get token uri empty of a no exists token id", async function() {
            await defaultNFT.mint(owner.address, ["1"]);

            await expect(defaultNFT.tokenURI(7)).to.be.revertedWith("ERC721Metadata: URI query for nonexistent token");
        });
    });

    describe("baseURI", function() {
        it("Should modify baseURI if I am owner", async function() {
            const newBaseURI = 'https://woper.maxpower/';

            await defaultNFT.setBaseURI(newBaseURI);

            expect(await defaultNFT.baseURI()).to.equal(newBaseURI);
        });

        it("Should not modify baseURI if I am not owner", async function() {
            await expect(defaultNFT.connect(addr1).setBaseURI("asdf"))
                .to.be.revertedWith('Ownable: caller is not the owner');
        });
    });

    describe("Transfers", function() {
        it("Should transfer my NFT", async function() {
            await defaultNFT.mint(owner.address, ["1"]);

            await defaultNFT.transferFrom(owner.address, addr1.address, 0);

            expect(await defaultNFT.ownerOf(0)).to.equal(addr1.address);
            expect(await defaultNFT.ownerOf(0)).to.not.equal(owner.address);
        });
    });
});
